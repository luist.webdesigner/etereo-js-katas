export function kata1 (number) {

    let arrayOfNumbers = number.toString().split("");
    let arraySwitched = arrayOfNumbers.sort(function(a, b){return b-a});
    return parseInt(arraySwitched.join(""));

}
