export function kata2 (obj, def, path) {

    if(path){
        return get(path)
    } else {
        return get;
    }

    function get(path) {
        let pathArray = path.split('.');

        let currentValue=obj;
        for(let nextPath of pathArray){
            currentValue = currentValue[nextPath];
            if(!currentValue){
                return def;
            }
        }
        return currentValue
    }

}
