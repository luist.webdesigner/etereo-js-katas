export function kata3 (n, m) {
    let result = new Array();

    let getDivisors = (n) => {
        let divisors = [1];
        for (let i=2;i <= n; i++) {
            if (n % i === 0) {
                divisors.push(i);
            }
        }
        return divisors;
    };

    let isSquared = (n) => {

        let divisors = getDivisors(n).map((divisor)=>{
            return Math.pow(divisor,2);
        });

        let sumed = divisors.reduce((sum,next) => {
            return sum + next;
        });

        if (Number.isInteger(Math.sqrt(sumed))){
            return [n,sumed];
        }else {
            return false;
        }
    };

    for(let i = n; i <= m; i++ ){
        let sum = isSquared(i);

        if(sum){
            result.push(sum)
        }
    }

    return result;

}
